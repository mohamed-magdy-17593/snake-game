import zip from 'ramda/src/zip'
import equals from 'ramda/src/equals'
import assocPath from 'ramda/src/assocPath'
import path from 'ramda/src/path'
import mathMod from 'ramda/src/mathMod'
import __ from 'ramda/src/__'
import sample from 'lodash/sample'
import pipe from 'ramda/src/pipe'

const SnakeBoardSize = 12
const gameSpeed = 150
const grayColor = '#303952'
const dodaColor = '#f5cd79'
const whiteColor = '#fff'

// =====
// impure code
// could use monads to wrap it to fill like pure interaction but i'm lazy :)
// dom change code
function createSnakeBoard(size) {
  const appDiv = document.getElementById('app')
  Array.from({length: size}).forEach((_, x) => {
    Array.from({length: size}).forEach((_, y) => {
      const div = document.createElement('div')
      div.setAttribute('id', `${x}-${y}`)
      appDiv.appendChild(div)
    })
  })
}

createSnakeBoard(SnakeBoardSize)

// will call it after every state change
function renderSnake(state) {
  const {snake, lose} = state
  if (lose) {
    alert('You are lose!!')
    actions.reset()
  } else {
    snake.forEach((a, x) => {
      a.forEach((i, y) => {
        document.getElementById(`${x}-${y}`).style.backgroundColor = i
          ? i == D
            ? dodaColor
            : grayColor
          : whiteColor
      })
    })
  }
}
// =====

// helper functions
const log = x => console.log(x) || x

const sum2ArrayWithTransformation = transformationCb => (arr1, arr2) =>
  zip(arr1, arr2).map(([n1, n2]) => transformationCb(n1 + n2))

const sum2Array = sum2ArrayWithTransformation(mathMod(__, SnakeBoardSize))

function getRandomEmptyIndex(snakeBord) {
  return sample(
    snakeBord
      .map((row, x) => row.map((i, y) => (i ? null : [x, y])))
      .flatMap(i => i)
      .filter(i => i),
  )
}

// in directions
// notation [x, y] mean
// directions
const D = 'DODA' // place Holder for Doda
const P = [0, 0] // placeholder no direction
const T = [-1, 0] // top direction
const B = [+1, 0] // bottom direction
const L = [0, -1] // left direction
const R = [0, +1] // right direction

// snake start state
const initialState = {
  head: [6, 6],
  tail: [6, 4],
  snake: [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, R, R, R, 0, 0, 0, D, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  ],
  directionQueue: [],
  direction: R,
  lose: false,
}

function setDirection(s) {
  let temp
  const {directionQueue, direction} = s
  return {
    ...s,
    direction: (function self() {
      return (temp = directionQueue.pop())
        ? // check if new direction is in reverse of old direction or the same
          equals(sum2Array(temp, direction), [0, 0]) || temp === direction
          ? self()
          : temp
        : direction
    })(),
    directionQueue,
  }
}

function getNewHeadPoint(s) {
  return {
    ...s,
    snake: assocPath(s.head, s.direction, s.snake),
    head: sum2Array(s.head, s.direction),
  }
}

function removeTailPointIfheadValueNotDoda(s) {
  const headValue = path(s.head, s.snake)
  if (headValue === D) {
    return {
      ...s,
      snake: assocPath(log(getRandomEmptyIndex(s.snake)), D, s.snake),
    }
  } else {
    return {
      ...s,
      snake: assocPath(s.tail, 0, s.snake),
      tail: sum2Array(s.tail, path(s.tail, s.snake)),
    }
  }
}

function setPlaceHolderToHeadValue(s) {
  return {
    ...s,
    lose: path(s.head, s.snake) && path(s.head, s.snake) !== D,
    snake: assocPath(s.head, P, s.snake),
  }
}

/**
 * state changer
 * @param {object} state
 * @param {{type: string, payload: any}} action
 */
function reducer(state = initialState, action) {
  switch (action.type) {
    case 'CHANGE_DIRECTION':
      return {
        ...state,
        directionQueue: [action.payload, ...state.directionQueue],
      }
    case 'MOVE':
      return pipe(
        setDirection,
        getNewHeadPoint,
        removeTailPointIfheadValueNotDoda,
        setPlaceHolderToHeadValue,
      )(state)
    case 'RESET':
      return initialState
    default:
      return state
  }
}

function createDispatcher(reducer, renderFn, state) {
  renderFn(state)
  return function dispatch(action) {
    state = reducer(state, action)
    renderFn(state)
  }
}

const dispatch = createDispatcher(reducer, renderSnake, initialState)

const actions = {
  move: () => dispatch({type: 'MOVE'}),
  changeDir: d => dispatch({type: 'CHANGE_DIRECTION', payload: d}),
  reset: () => dispatch({type: 'RESET'}),
}

// for testing
window.actions = actions
window.T = T
window.B = B
window.L = L
window.R = R

// game play
setInterval(actions.move, gameSpeed)

// keys events
document.addEventListener(
  'keydown',
  e => {
    switch (e.keyCode) {
      case 37:
        actions.changeDir(L)
        break
      case 38:
        actions.changeDir(T)
        break
      case 39:
        actions.changeDir(R)
        break
      case 40:
        actions.changeDir(B)
        break
    }
  },
  false,
)
